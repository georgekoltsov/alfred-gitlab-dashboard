# alfred-gitlab

Alfred (https://www.alfredapp.com/) workflow to view GitLab Issues/Merge Requests/ToDos that are assigned to you.

![Main screen](./images/doc/main.png)

## Installation 

Download and double click latest workflow from [releases page](https://gitlab.com/georgekoltsov/alfred-gitlab/-/releases)

## Setup 

In order for workflow to work, 3 things need to be configured:

- URL. Defaults to https://gitlab.com
- Personal access token. ⚠️ Stored as plain text in `config.json` ⚠️
- Username. To be used in search queries. This setting will eventually be deprecated in favour of automatic username lookup

![Main screen](./images/doc/configure.png)

## Usage

Available commands:

### `gli` - List issues

![List issues](./images/doc/issues.png)

Query: `?assignee_username=<username>&scope=all&state=opened&utf8=✓`

### `glm` - List merge requests

![List merge requests](./images/doc/merge_requests.png)

Query: `?assignee_username=<username>&scope=all&state=opened&utf8=✓`

### `glt` - List todos

![List todos](./images/doc/todos.png)

Query: `?state=pending`

## Contributing

See [CONTRIBUTING](./CONTRIBUTING.md)

## License

See [LICENSE](./LICENSE)
