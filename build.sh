version=$( cat VERSION )

workflow_name=alfred-gitlab-$version

cd src; zip -r ../$workflow_name.zip *
cd ../images; zip -u ../$workflow_name.zip -j icon.png
cd ../workflow; zip -u ../$workflow_name.zip -j info.plist
cd ..
zip -u $workflow_name.zip -j VERSION
mv $workflow_name.zip $workflow_name.alfredworkflow

