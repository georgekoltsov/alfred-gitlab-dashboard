class ResourceCommand
  attr_reader :items

  def initialize(resource:, resource_fields:, arg: nil)
    @resource        = resource
    @resource_fields = resource_fields
    @arg             = arg
    @items           ||= Items.new
  end

  def run
    STDOUT.write(items)
  end

  def items
    Items.new(resource).to_json
  end

  def resource
    GitlabService.new.get_resource(@resource).map(&method(:convert_to_item))
  end

  def convert_to_item(resource)
    Item.new(
      title:    resource_field_for(:title, resource),
      subtitle: resource_field_for(:subtitle, resource),
      arg:      resource_field_for(:arg, resource),
      valid:    resource_field_for(:valid, resource)
    ).to_h
  end

  def resource_field_for(field_name, resource)
    case @resource_fields[field_name]
    when Array
      resource.dig(*@resource_fields[field_name])
    when Proc
      @resource_fields[field_name].call(resource)
    else
      @resource_fields[field_name]
    end
  end
end
