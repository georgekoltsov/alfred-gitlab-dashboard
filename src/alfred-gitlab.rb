require 'json'
require 'forwardable'
require 'net/http'

require_relative 'workflow'
require_relative 'config'
require_relative 'command_factory'
require_relative 'items'
require_relative 'item'
require_relative 'gitlab_service'

Workflow.new(ARGV).run
