class ConfigureCommand
  attr_reader :keyword, :value, :args, :items, :needs_update

  def initialize(args)
    @args         = args[0].split(' ')
    @needs_update = !args[1].nil?
    @keyword      = @args[0]
    @value        = @args[1]
    @config       = Config.load
  end

  def available_options
    @available_options ||= %w(url token username)
  end

  def run
    return update_config if needs_update
    return out(all_items) unless keyword

    out(filtered_items)
  end

  def all_items
    Items.new([url_item, token_item, username_item]).to_json
  end

  def filtered_items
    items = Items.new

    available_options.each do |option|
      valid = value ? true : false

      items << public_send("#{option}_item", valid) if option.include?(keyword)
    end

    items.to_json
  end

  def username_item(valid = false)
    Item.new(
      title:        'username',
      subtitle:     "GitLab Username (to be used in search queries). Current value  #{@config.username}",
      autocomplete: 'username ',
      arg:          "username #{value}",
      valid:        valid
    ).to_h
  end

  def url_item(valid = false)
    Item.new(
      title:        'url',
      subtitle:     "GitLab URL. Current value: #{@config.url}",
      autocomplete: 'url ',
      arg:          "url #{value}",
      valid:        valid
    ).to_h
  end

  def token_item(valid = false)
    Item.new(
      title:        'token',
      subtitle:     "GitLab API Token (stored in config.json as plain text). Current value: #{@config.token}",
      autocomplete: 'token ',
      arg:          "token #{value}",
      valid:        valid
    ).to_h
  end

  def update_config
    @config.update({ keyword => value })
  end

  def out(content)
    STDOUT.write(content)
  end
end
