class IssuesCommand
  extend Forwardable

  def_delegator :@resource_command, :run, :run

  def initialize(arg)
    @resource_command = ResourceCommand.new(
      resource:        :issues,
      resource_fields: resource_fields,
      arg:             arg
    )
  end

  private

  def resource_fields
    {
      title:    %w(title),
      subtitle: %w(references full),
      arg:      %w(web_url),
      valid:    true
    }
  end
end
