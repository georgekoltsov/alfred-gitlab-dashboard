require_relative 'resource_command'
require_relative 'configure_command'
require_relative 'issues_command'
require_relative 'merge_requests_command'
require_relative 'todos_command'

class CommandFactory
  def self.from_args(args)
    type = args.shift

    case type
    when 'configure'
      ConfigureCommand.new(args)
    when 'issues'
      IssuesCommand.new(args)
    when 'merge_requests'
      MergeRequestsCommand.new(args)
    when 'todos'
      TodosCommand.new(args)
    end
  end
end
