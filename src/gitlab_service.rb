class GitlabService
  def initialize
    @config   = Config.load
    @host     = @config.url
    @username = @config.username
  end

  def get_resource(resource)
    JSON.parse(request(resource_url(resource)))
  end

  def resource_url(resource)
    URI.join(@host, 'api/', 'v4/', resource.to_s, "?#{resource_query_string(resource)}")
  end

  private

  def request(url)
    res = Net::HTTP.get_response(URI(url))

    if res.is_a?(Net::HTTPSuccess)
      res.body
    else
      [].to_s
    end
  end

  def resource_query_string(resource)
    q = { access_token: @config.token }

    case resource
    when :todos
      q[:state] = 'pending'
    when :issues, :merge_requests
      q[:assignee_username] = @config.username
      q[:scope]             = 'all'
      q[:state]             = 'opened'
      q[:utf8]              = '✓'
    end

    URI.encode_www_form(q)
  end
end
