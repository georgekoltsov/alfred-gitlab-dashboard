class Items
  attr_reader :items

  def initialize(items = [])
    @items = { items: items }
  end

  def <<(item)
    @items[:items] << item
  end

  def to_json
    @items.to_json
  end
end
