class TodosCommand
  extend Forwardable

  def_delegator :@resource_command, :run, :run

  def initialize(arg)
    @resource_command = ResourceCommand.new(
      resource:        :todos,
      resource_fields: resource_fields,
      arg:             arg
    )
  end

  private

  def resource_fields
    {
      title:    ->(resource) { "#{resource['author']['name']} #{resource['action_name']} on #{resource['target_type']}" },
      subtitle: %w(body),
      arg:      %w(target_url),
      valid:    true
    }
  end
end
