class Item
  attr_accessor :uid, :title, :subtitle, :arg, :icon_path, :valid, :autocomplete, :type

  def initialize(opts = {})
    opts.each do |key, value|
      public_send("#{key}=", value) if respond_to?("#{key}=")
    end
  end

  def to_h
    {
      uid:          uid,
      title:        title,
      subtitle:     subtitle,
      arg:          arg,
      icon:         {
        path: icon_path
      },
      valid:        valid,
      autocomplete: autocomplete,
      type:         type
    }
  end

  def icon_path
    @icon_path || 'icon.png'
  end

  def to_json
    to_h.to_json
  end
end
