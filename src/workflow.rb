class Workflow
  extend Forwardable

  def_delegator :@command, :run, :run

  def initialize(args)
    @command = CommandFactory.from_args(args)
  end
end
