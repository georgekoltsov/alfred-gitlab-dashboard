class Config
  attr_reader :url, :token, :username

  def self.load
    self.new.load
  end

  def initialize
    @url      = nil
    @token    = nil
    @username = nil
  end

  def load
    ensure_config

    config = JSON.parse(File.open(config_filename).read)

    @url      = config.dig('url') || 'https://gitlab.com/'
    @token    = config.dig('token') || ''
    @username = config.dig('username') || ''

    self
  end

  def update(attributes)
    attributes.each do |key, value|
      var_name = :"@#{key.to_s}"

      begin
        self.instance_variable_set(var_name, value) if self.instance_variable_get(var_name)
      rescue => _
        continue
      end
    end

    save
  end

  def save
    File.write(config_filename, to_json)
  end

  def to_json
    {
      url:      @url,
      token:    @token,
      username: @username
    }.to_json
  end

  def ensure_config
    return if File.exists?(config_filename)

    File.write(config_filename, empty_config.to_json)
  end

  def empty_config
    {
      url:      'https://gitlab.com',
      token:    '',
      username: ''
    }
  end

  def config_filename
    'config.json'
  end
end
